package com.a1011011gmail.youssef.android_tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    BookDbHelper Bookdb;
    SimpleCursorAdapter adapter;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Instance BookDbHelper
        Bookdb = new BookDbHelper(this);
        //Bookdb.populate();
        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Button Plus
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        final Intent in = new Intent(this, InsertBookActivity.class);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(in);

            }
        });




        // Declare cursor
        final Cursor cur = Bookdb.fetchAllBooks();

        //Parametre of SimpleCursorAdapter --> from and to hadeek a7alof
        String[] from = new String[] {Bookdb.COLUMN_BOOK_TITLE,Bookdb.COLUMN_AUTHORS};
        int[] to = new int[] {android.R.id.text1,android.R.id.text2};
        adapter = new SimpleCursorAdapter(this,android.R.layout.simple_list_item_2,cur,from,to,0);
        lv = (ListView) findViewById(R.id.listview);

        lv.setAdapter(adapter);
        registerForContextMenu(lv);
        adapter.notifyDataSetChanged();
        final Intent intent = new Intent(this, BookActivity.class);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                cur.moveToPosition(position);
                int rowId = cur.getInt(cur.getColumnIndexOrThrow("_id"));

                Bundle b = new Bundle();
                b.putInt("var", rowId);
                intent.putExtra("in",b);
                startActivity(intent);



            }
        });

    }

    public void showMessage(String title , String message){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();

    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final Cursor cur = Bookdb.fetchAllBooks();
        cur.moveToPosition(info.position);
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            showMessage("Message","Suppression Succes");
            Bookdb.deleteBook(cur);
            adapter.changeCursor(Bookdb.getReadableDatabase().query(Bookdb.TABLE_NAME, new String[] {"ROWID AS _id",Bookdb.COLUMN_BOOK_TITLE,Bookdb.COLUMN_AUTHORS},null, null, null, null, null));
            adapter.notifyDataSetChanged();
            return true;
        }

        return super.onContextItemSelected(item);
    }


}
