package com.a1011011gmail.youssef.android_tp2;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InsertBookActivity extends AppCompatActivity {

    BookDbHelper Bookdb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_book);

        // Instance BookDbHelper
        Bookdb = new BookDbHelper(this);

        final EditText Nom = (EditText)findViewById(R.id.nameBook);
        final EditText Auteur = (EditText)findViewById(R.id.editAuthors);
        final EditText Annee = (EditText)findViewById(R.id.editYear);
        final EditText Genre = (EditText)findViewById(R.id.editGenres);
        final EditText Publier = (EditText)findViewById(R.id.editPublisher);

        Button Sauv = (Button)findViewById(R.id.button);

        final Intent i = new Intent(this,MainActivity.class);


        Sauv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String n = String.valueOf(Nom.getText());
                String a = String.valueOf(Auteur.getText());
                String an = String.valueOf(Annee.getText());
                String g = String.valueOf(Genre.getText());
                String p = String.valueOf(Publier.getText());

                //check
                String check = Nom.getText().toString();
                //Cursor
                Cursor cur = Bookdb.fetchAllBooks();


                    if(check.matches("")){

                        showMessage("Insersion Impossible","Le titre de livre doit étre non vide.");
                    }

                 else {

                    if (Bookdb.addBook(new Book(n, a, an, g, p)) == true) {

                        showMessage("Message", "Insrerer avec succes");
                        startActivity(i);
                    } else {
                        showMessage("Error", "Erreur d'insertion");
                    }

                }
            }
        });
    }

    public void showMessage(String title , String message){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();

    }
}
