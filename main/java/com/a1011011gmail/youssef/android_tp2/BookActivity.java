package com.a1011011gmail.youssef.android_tp2;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class BookActivity extends AppCompatActivity {

    BookDbHelper Bookdb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        // Instance BookDbHelper
        Bookdb = new BookDbHelper(this);

        final EditText Nom = (EditText)findViewById(R.id.nameBook);
        final EditText Auteur = (EditText)findViewById(R.id.editAuthors);
        final EditText Annee = (EditText)findViewById(R.id.editYear);
        final EditText Genre = (EditText)findViewById(R.id.editGenres);
        final EditText Publier = (EditText)findViewById(R.id.editPublisher);
        final Button Sauv = (Button)findViewById(R.id.button);

        //get Id book
        Intent intent = getIntent();
        Bundle bn = intent.getBundleExtra("in");
        final int id = bn.getInt("var");

        Cursor rs = Bookdb.getData(id);
        rs.moveToFirst();
        String n = rs.getString(rs.getColumnIndex(Bookdb.COLUMN_BOOK_TITLE));
        String au = rs.getString(rs.getColumnIndex(Bookdb.COLUMN_AUTHORS));
        String a = rs.getString(rs.getColumnIndex(Bookdb.COLUMN_YEAR));
        String g =  rs.getString(rs.getColumnIndex(Bookdb.COLUMN_GENRES));
        String p = rs.getString(rs.getColumnIndex(Bookdb.COLUMN_PUBLISHER));



        Nom.setText(n);
        Auteur.setText(au);
        Annee.setText(a);
        Genre.setText(g);
        Publier.setText(p);
        final Intent i = new Intent(this,MainActivity.class);

        Sauv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String upn = String.valueOf(Nom.getText());
                String upau = String.valueOf(Auteur.getText());
                String upa = String.valueOf(Annee.getText());
                String upg = String.valueOf(Genre.getText());
                String upp = String.valueOf(Publier.getText());

                //check
                String check = Nom.getText().toString();

                if(check.matches("")){

                    showMessage("Sauvegarde impossible","Le titre de livre doit étre non vide.");
                }
                else{

                    int res = Bookdb.updateBook(new Book(id,upn,upau,upa,upg,upp));

                    if(res > 0 ){

                        showMessage("Message", "Update succes");
                        startActivity(i);
                    }
                    else{

                        showMessage("Message", "Error update");

                    }

                }
            }
        });
    }

    public void showMessage(String title , String message){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();

    }
}
