package com.a1011011gmail.youssef.android_tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class BookDbHelper extends SQLiteOpenHelper {

    private static final String TAG = BookDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "book.db";

    public static final String TABLE_NAME = "library";

    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";

    public BookDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        // db.execSQL() with the CREATE TABLE ... command
        db.execSQL(" CREATE TABLE "+ TABLE_NAME + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT , title TEXT, authors TEXT, year NUMERIC, genres TEXT, publisher TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("drop table if exists " + TABLE_NAME);
        onCreate(db);

    }


    /**
     * Adds a new book
     * @return  true if the book was added to the table ; false otherwise
     */
    public boolean addBook(Book book) {
        Log.e(TAG,"add book");
        SQLiteDatabase db = this.getWritableDatabase();
        // Inserting Row
        long rowID = 0;
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_BOOK_TITLE, book.getTitle());
        contentValues.put(COLUMN_AUTHORS, book.getAuthors());
        contentValues.put(COLUMN_YEAR, book.getYear());
        contentValues.put(COLUMN_GENRES, book.getGenres());
        contentValues.put(COLUMN_PUBLISHER, book.getPublisher());
        // call db.insert()
        db.insert(TABLE_NAME, null, contentValues);
        // Closing database connection
        db.close();
        return (rowID != -1);
    }

    /**
     * Updates the information of a book inside the data base
     * @return the number of updated rows
     */
    public int updateBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        int res = 0;

        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_BOOK_TITLE, book.getTitle());
        contentValues.put(COLUMN_AUTHORS, book.getAuthors());
        contentValues.put(COLUMN_YEAR, book.getYear());
        contentValues.put(COLUMN_GENRES, book.getGenres());
        contentValues.put(COLUMN_PUBLISHER, book.getPublisher());
        // updating row
        res = db.update(TABLE_NAME, contentValues, "_id = ? ", new String[]{Long.toString(book.getId())});
        // call db.update()
        return res;
    }


    public Cursor fetchAllBooks() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME, null);
        // call db.query()
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public void deleteBook(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        // call db.delete();
        db.delete(TABLE_NAME,_ID + " = ?", new String[] { String.valueOf(cursor.getLong(0)) });
        db.close();
    }
    public void populate() {
        Log.d(TAG, "call populate()");
        addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    public static Book cursorToBook(Cursor cursor) {
        Book book = new Book();
        // build a Book object from cursor
        book.setId(cursor.getLong(0));
        book.setTitle(cursor.getString(1));
        book.setAuthors(cursor.getString(2));
        book.setYear(cursor.getString(3));
        book.setGenres(cursor.getString(4));
        book.setPublisher(cursor.getString(5));

        return book;
    }


    //Get data with id
    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+ TABLE_NAME +" where "+_ID +" = " + id, null );
        return res;
    }




}
